info:
  tool: Jenkins
  language: Jenkins/DSL based on Groovy
  description: |
    Implementa as funcionalidades do pipeline na ferramenta Jenkins.
    Essas funcionalidades são referenciadas em arquivos de desrcição de
    pipelines pelos componentes "appliers"

vars:
  token: dev2ops
  src_dir: .
  mvn_default_settings: true
  mvn_build_container: maven38j11
  mvn_sonar_container: maven38j11

functions:
  build-maven:
    description: Função que compila o código java usando o maven
    debug: true
    args:
    - name: caller_path
      type: applier
      description: Caminho do componente a ser compilado
      default: ${_self}
    - name: src_dir
      description: Caminho onde o arquivo pom está localizado
      type: scalar
      default: ${src_dir}
    - name: mvn_container
      type: config
      params:
        default: maven
      description: Container maven para compilar projetos java
      default: ${mvn_build_container}
    - name: mvn_default_settings
      type: scalar      
      description: Se true será utilizado o settings configurado no framework, se false utilizirá o do container
      default: ${mvn_default_settings}
    code: |
      container( '{{ mvn_container }}' ){
        sh 'echo mvn_default_settings: {{ mvn_default_settings }}'
        sh 'echo mvn_container: {{ mvn_container }}'

        configFileProvider(
          [configFile(fileId: 'settings.xml', variable: 'SETTINGS')]) {
          dir ( "{{ caller_path }}/{{ src_dir }}" ){   
              if({{ mvn_default_settings }}){
                sh 'mvn -s $SETTINGS -q -Dmaven.repo.local=m2 compile'
              } else {
                sh 'mvn -q -Dmaven.repo.local=m2 compile'
              }
          }
        }    
      }

  test-maven:
    description: Função que testa o código java usando o maven
    debug: true
    args:
    - name: caller_path
      type: applier
      description: Caminho do componente a ser compilado
      default: ${_self}
    - name: src_dir
      description: Caminho onde o arquivo pom está localizado
      type: scalar
      default: ${src_dir}
    - name: mvn_container
      type: config
      params:
        default: maven
      description: Container maven para compilar projetos java
      default: ${mvn_build_container}
    - name: mvn_default_settings
      type: scalar      
      description: Se true será utilizado o settings configurado no framework, se false utilizirá o do container
      default: ${mvn_default_settings}
    code: |
      container( '{{ mvn_container }}' ){
        configFileProvider(
          [configFile(fileId: 'settings.xml', variable: 'SETTINGS')]) {
          dir ( "{{ caller_path }}/{{ src_dir }}" ){
              if({{ mvn_default_settings }}){
                sh 'mvn -s $SETTINGS -q -Dmaven.repo.local=m2 test'
              } else {
                sh 'mvn -q -Dmaven.repo.local=m2 test'
              }             
          }
        }    
      }

  sonar-maven:
    description: Função que testa o código java usando o maven
    debug: true
    args:
    - name: caller_path
      type: applier
      description: Caminho do componente a ser compilado
      default: ${_self}
    - name: src_dir
      description: Caminho onde o arquivo pom está localizado
      type: scalar
      default: ${src_dir}
    - name: sonarqube_credential
      type: config
      params:
        default: sonarqube
      description: Endpoint da rest API do influxdb
      default: sonarqube.credential.id
    - name: sonarqube_endpoint
      type: config
      params:
        default: sonarqube.example.com
      description: Endpoint da rest API do sonarqube
      default: sonarqube.http.endpoint      
    - name: sonarqube_credential
      type: config
      params:
        default: sonarqube
      description: Endpoint da rest API do sonarqube
      default: sonarqube.credential.id
    - name: mvn_container
      type: config
      params:
        default: maven
      description: Container maven para compilar projetos java
      default: ${mvn_sonar_container}
    - name: mvn_default_settings
      type: scalar      
      description: Se true será utilizado o settings configurado no framework, se false utilizirá o do container
      default: ${mvn_default_settings}
    - name: sonarqube_project_key
      type: scalar
      description: Chave do projeto no Sonar
      default: ${sonar_project_key}
    code: |
      container( '{{ mvn_container }}' ){
        withCredentials([
            usernamePassword(credentialsId: "{{ sonarqube_credential }}", usernameVariable: 'SONAR_USR', passwordVariable: 'SONAR_PSW')
        ]){
          dir ( "{{ caller_path }}/{{ src_dir }}" ){
            configFileProvider(
              [configFile(fileId: 'settings.xml', variable: 'SETTINGS')]) {
              dir ( "{{ caller_path }}/{{ src_dir }}" ){
                if({{ mvn_default_settings }}){
                  sh 'SONAR_USER_HOME=/tmp/.sonar mvn -s $SETTINGS -q -Dmaven.repo.local=m2 sonar:sonar -Dsonar.host.url="{{ sonarqube_endpoint }}" -Dsonar.login=$SONAR_PSW -Dsonar.projectKey="{{ sonarqube_project_key }}"'
                } else {
                  sh 'SONAR_USER_HOME=/tmp/.sonar mvn -q -Dmaven.repo.local=m2 sonar:sonar -Dsonar.host.url="{{ sonarqube_endpoint }}" -Dsonar.login=$SONAR_PSW -Dsonar.projectKey="{{ sonarqube_project_key }}"'
                } 
              }
            }    
          }        
        }
      }

  package-maven:
    description: Função que empacota o código maven
    debug: true
    args:
    - name: caller_path
      type: applier
      description: Caminho do componente a ser compilado
      default: ${_self}
    - name: src_dir
      description: Caminho onde o arquivo pom está localizado
      type: scalar
      default: ${src_dir}
    - name: mvn_container
      type: config
      params:
        default: maven
      description: Container maven para compilar projetos java
      default: ${mvn_build_container}
    - name: mvn_default_settings
      type: scalar      
      description: Se true será utilizado o settings configurado no framework, se false utilizirá o do container
      default: ${mvn_default_settings}
    code: |
      container( '{{ mvn_container }}' ){
        dir ( "{{ caller_path }}/{{ src_dir }}" ){
          configFileProvider(
            [configFile(fileId: 'settings.xml', variable: 'SETTINGS')]) {
            dir ( "{{ caller_path }}/{{ src_dir }}" ){
              if({{ mvn_default_settings }}){
                sh 'mvn -s $SETTINGS -q -Dmaven.repo.local=m2 package -Dmaven.main.skip -DskipTests'
              } else {
                sh 'mvn -q -Dmaven.repo.local=m2 package -Dmaven.main.skip -DskipTests'
              } 
            }
          }    
        }
      }

